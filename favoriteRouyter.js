const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var authenticate = require('../authenticate');

const Favorites = require('../models/favorite');

const favoriteRouter = express.Router();

favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
.get(authenticate.verifyUser, (req, res, next) => {
	Favorites.find({user: req.user._id})
	.populate('user')
	.populate('favorites')
	.then((favorites) => {
		res.statusCode = 200;
		res.setHeader('Content-Type', 'application/json');
		res.json(favorites);
	}, (err) => next(err))
	.catch((err) => next(err));
})

.post(authenticate.verifyUser, (req, res, next) => {
Favorites.findOne({ user: req.user._id })
	.then((favorites) => {
		if (favorites == null) {
			Favorites.create({})
				.then((favorites) => {
					favorites.user = req.user._id;
					favorites.dishes = req.body.dishes;
					favorites.save()
					.then((favorite) => {
						res.statusCode = 200;
						res.setHeader('Content-Type', 'application/json');
						res.json(favorite);
					})
					.catch(err => next(err));
				})
				.catch(err => next(err));
		} else {
			for (var x = 0; x < req.body.length; x++) {
				dishid = req.body[x]._id;
				if (favorites.dishes.indexOf(dishid) === -1) {
					favorites.dishes.push(dishid);
				}
			}
			favorites.user = req.user._id;
			favorites.save()
			.then((favorite) => {
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				res.json(favorite);
			})
			.catch(err => next(err));
		}
	})
	.catch(err => next(err));
})
.put(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
	res.statusCode = 403;
	res.end('PUT operation not supported on /favorites');
})
.delete(authenticate.verifyUser, (req, res, next) => {
	Favorites.findOneAndRemove({user: req.user._id}) 
	.then((resp) => {
		res.statusCode = 200;
		res.setHeader('Content-Type', 'application/json');
		res.json(resp);
	}, (err) => next(err))
	.catch((err) => next(err));
});

favoriteRouter.route('/:dishId')
.get(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
	res.statusCode = 403;
	res.end('GET operation not supported on /favorites/ ' + req.params.dishId);
})

.post(authenticate.verifyUser, (req, res, next) => {
	console.log('User: ', req.user._id, 'Fave: ', req.params.dishId)
	Favorites.findById(req.user._id)

	.then((favorite) => {
		if (favorite != null) {
			if (favorites.dishes.indexOf(req.params.dishId) == -1) {
				favorites.dishes.push(req.params.dishId);
				favorites.save()
					.then((favorites) => {
						res.json(favorites);
					}, (err) => next(err));
			} else {
				console.log('Favorite exists!');
				res.json(favorites);
			}
		}
		else if (favorite == null) {
			// Create new favorite list
			var fave = {user: req.user._id, favorites: [ {_id: req.params.dishId} ]};
		
			Favorites.create(fave)
			.then((favorite) => {
				console.log('Favorites Created ', favorite);
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				res.json(favorite);
			}, (err) => next(err));
		}
	}, (err) => next(err))
	.catch((err) => next(err));
})
.put(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
	res.statusCode = 403;
	res.end('PUT operation not supported on /favorites/ ' + req.params.dishId);
})

.delete(authenticate.verifyUser, (req, res, next) => {
	Favorites.findOne({user: req.user._id})
	.populate('user')
	.populate('favorites')
	.then((favorite) => {
		if (favorite != null) {
			favorite.favorites.id(req.params.dishId).remove();
			favorite.save()
			.then((favorite) => {
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				res.json(favorite);				
			}, (err) => next(err));
		}
		else {
			err = new Error('Favorite ' + req.params.favoriteId + ' not found');
			err.status = 404;
			return next(err);
		}
	}, (err) => next(err))
	.catch((err) => next(err));	
});

module.exports = favoriteRouter;